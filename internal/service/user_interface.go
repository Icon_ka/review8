package service

import (
    "context"
    "awesomeProject8/internal/entities"
)

type UserServicer interface {
    Reg(ctx context.Context, user entities.User) error
}
