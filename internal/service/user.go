package service

import (
    "awesomeProject8/internal/repository"
    "awesomeProject8/internal/entities"
    "context"
    "errors"
    "database/sql"
)

type UserService struct {
    repo repository.UserRepositorer
}

func NewUserService(pg *sql.DB) *UserService {
    return &UserService{repo: repository.NewUserRepo(pg)}
}

func (s UserService) Reg(ctx context.Context, user entities.User) error {
    if user.Age < 18 {
        return errors.New("age < 18")
    }
    exist, err := s.repo.Exists(ctx, user.Email)
    if err != nil {
        return err
    }
    if exist {
        return errors.New("email already exist")
    }

    err = s.repo.Register(ctx, user)
    if err != nil {
        return err
    }
    return nil
}
