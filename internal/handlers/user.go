package handlers

import (
    "net/http"
    "awesomeProject8/internal/service"
    "awesomeProject8/internal/entities"
    "encoding/json"
    "context"
    "log"
    "fmt"
)

type UserHandler struct {
    service service.UserServicer
}

func NewUserHandler(service service.UserServicer) *UserHandler {
    return &UserHandler{service: service}
}

func (u *UserHandler) Register(writer http.ResponseWriter, request *http.Request) {
    var user entities.User

    err := json.NewDecoder(request.Body).Decode(&user)
    if err != nil {
        log.Print(err)
        writer.WriteHeader(http.StatusBadRequest)
        writer.Write([]byte("Ошибка при сериализации"))
        return
    }

    err = u.service.Reg(context.Background(), user)
    if err != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        writer.Write([]byte(fmt.Sprintf("Ошибка при запросе к базе данных: %v", err)))
        return
    }

    writer.WriteHeader(http.StatusOK)
    writer.Write([]byte("Пользователь зарегистрирован"))
}
