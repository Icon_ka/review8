package handlers

import "net/http"

type UserHandlerer interface {
    Register(http.ResponseWriter, *http.Request)
}
