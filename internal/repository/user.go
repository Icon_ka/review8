package repository

import (
    "database/sql"
    "awesomeProject8/internal/entities"
    "context"
    "log"
    "fmt"
)

type PostgersUserRepo struct {
    pg *sql.DB
}

func NewUserRepo(pg *sql.DB) *PostgersUserRepo {
    return &PostgersUserRepo{pg: pg}
}
func (r PostgersUserRepo) Register(ctx context.Context, user entities.User) error {
    dUs := user
    fmt.Println(dUs)
    q := `INSERT INTO users (name, email, password, age) VALUES ($1, $2, $3, $4)`

    _, err := r.pg.ExecContext(ctx, q, user.Name, user.Email, user.Password, user.Age)
    if err != nil {
        log.Print(err)
        return err
    }

    return nil
}

func (r PostgersUserRepo) Exists(ctx context.Context, email string) (bool, error) {
    query := `SELECT EXISTS(SELECT * FROM users WHERE email = $1)`

    var exist bool
    err := r.pg.QueryRowContext(ctx, query, email).Scan(&exist)
    if err != nil {
        log.Print(err)
        return false, err
    }

    return exist, nil
}
