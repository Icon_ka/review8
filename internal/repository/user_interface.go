package repository

import (
    "awesomeProject8/internal/entities"
    "context"
)

type UserRepositorer interface {
    Register(ctx context.Context, user entities.User) error
    Exists(ctx context.Context, email string) (bool, error)
}
