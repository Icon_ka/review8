package main

import (
    "github.com/go-chi/chi"
    "awesomeProject8/internal/handlers"
    "log"
    "database/sql"
    "fmt"
    "awesomeProject8/internal/service"
    "net/http"
    _ "github.com/lib/pq"
)

func main() {
    log.Print("Connect database")
    db, err := sql.Open("postgres", fmt.Sprintf("postgres://alex:12345@db:5432/postgres?sslmode=disable"))

    if err != nil {
        log.Fatalf("Failed to open database: %v", err)
    }
    serv := service.NewUserService(db)
    ctrl := handlers.NewUserHandler(serv)

    r := chi.NewRouter()

    r.Post("/register", ctrl.Register)

    http.ListenAndServe(":8080", r)
}
