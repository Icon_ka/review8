module awesomeProject8

go 1.19

require (
	github.com/go-chi/chi v1.5.5
	github.com/golang-migrate/migrate/v4 v4.16.2
	github.com/jmoiron/sqlx v1.3.5
)

require (
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/lib/pq v1.10.2 // indirect
	go.uber.org/atomic v1.7.0 // indirect
)
